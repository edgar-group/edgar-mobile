import {API_URL} from '../config';

function status(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  }

  return Promise.reject(response);
}

export function post(url, options = {}) {
  const fetchUrl = url.startsWith('/') ? url : `/${url}`;

  return fetch(`${API_URL}${fetchUrl}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    body: JSON.stringify(options.data),
    credentials: 'same-origin'
  })
  .then(status)
  .then((response) => response.json());
}

export function get(url, options = {}) {
  const fetchUrl = url.startsWith('/') ? url : `/${url}`;

  return fetch(`${API_URL}${fetchUrl}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    credentials: 'same-origin'
  })
  .then(status)
  .then((response) => response.json());
}

export default {
  post,
  get
};
