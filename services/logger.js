import { Alert } from 'react-native';
import { post } from './request';

class Logger {
  sendHB() {
    return post('/test/hb').then((response) => {
      if (response.error) {
        Alert.alert('An error occured', response.error);
      }
    });
  }

  sendLog(eventName, eventData) {
    return post('/test/log', {
      data: {
        e: {eventName, eventData}
      }
    });
  }
}

export default new Logger();
