import React, { Component } from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native';
import { observer } from 'mobx-react/native';

import LoginForm from '../components/forms/LoginForm';
import LoginPinForm from '../components/forms/LoginPinForm';
import colors from '../styles/colors';
import {PIN_LOGIN_ENABLED} from '../config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgPrimary,
    alignItems: 'center',
    alignSelf: 'stretch'
  },
  header: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  subtitle: {
    fontSize: 20,
    color: colors.textPrimaryInverted
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold',
    color: colors.textPrimaryInverted
  },
  main: {
    flex: 4
  }
});

@observer
export default class LoginScreen extends Component {
  render() {

    const form = PIN_LOGIN_ENABLED ? (<LoginPinForm />) : (<LoginForm />);

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <View style={styles.header}>
          <Text style={styles.subtitle}>Welcome to</Text>
          <Text style={styles.title}>EDGAR</Text>
        </View>
        <View style={styles.main}>
          {form}
        </View>
      </KeyboardAvoidingView>
    );
  }
}
