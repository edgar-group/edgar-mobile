import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, KeyboardAvoidingView } from 'react-native';

import NewTestForm from '../components/forms/NewTestForm';
import colors from '../styles/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgLight,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch'
  },
  intro: {
    color: colors.textPrimary
  }
});

export default class MainScreen extends Component {
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <StatusBar barStyle="dark-content" />
        <Text style={styles.intro}>READY TO WRITE A TEST?</Text>
        <NewTestForm />
      </KeyboardAvoidingView>
    );
  }
}
