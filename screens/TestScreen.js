import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, Button, ActivityIndicator, Alert } from 'react-native';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react/native';

import colors from '../styles/colors';
import Question from '../components/Question';
import logger from '../services/logger';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignSelf: 'stretch'
  },

  loading: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  },

  header: {
    marginTop: 20,
    flexDirection: 'column'
  },

  studentName: {
    textAlign: 'center'
  },

  headerContent: {
    height: 50,
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.borderPrimary,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },

  headerText: {
    fontSize: 18
  },

  main: {
    flex: 1
  },

  footer: {
    height: 50,
    padding: 10,
    marginBottom: 10,
    borderTopWidth: 0.5,
    borderTopColor: colors.borderPrimary,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },

  timer: {
    fontSize: 20
  },

  red: {
    backgroundColor: colors.bgImportant,
    color: colors.textPrimaryInverted
  }
});

@inject('testStore')
@observer
@reactMixin.decorate(TimerMixin)
export default class TestScreen extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      formatSeconds: ''
    };
  }

  formatTime(time) {
    function formatNumber(number) {
      return Math.abs(number) < 10 ? `0${number}` : `${number}`;
    }

    const prefix = time < 0 ? '-' : '';
    const timeToFormat = Math.abs(time);
    const ONE_HOUR = 60 * 60;
    const ONE_DAY = 24 * ONE_HOUR;
    const days = Math.floor(timeToFormat / ONE_DAY);
    const hours = formatNumber(Math.floor((timeToFormat % ONE_DAY) / ONE_HOUR));
    const minutes = formatNumber(Math.floor((timeToFormat % ONE_HOUR) / 60));
    const seconds = formatNumber(Math.abs(timeToFormat % 60));
    if (days) {
      const unit = days === 1 ? 'day' : 'days';
      return `${prefix}${days} ${unit} ${hours}:${minutes}:${seconds}`;
    }
    return `${prefix}${hours}:${minutes}:${seconds}`;
  }

  previousQuestion() {
    this.props.testStore.goToPreviousQuestion();
  }

  nextQuestion() {
    this.props.testStore.goToNextQuestion();
  }

  submitTest() {
    Alert.alert('Test submit', 'Are you sure you want to submit this test?', [{
      text: 'Yes',
      onPress: () => {
        this.props.testStore.submit();
      }
    }, {
      text: 'No'
    }]);
  }

  startHB() {
    const ONE_MINUTE = 60 * 1000;
    this.setInterval(() => {
      this.sendHB();
    }, ONE_MINUTE);
  }

  sendHB() {
    return logger.sendHB();
  }

  startTimer() {
    this.formatRemainingTime();

    this.setInterval(() => {
      this.formatRemainingTime();
    }, 1000);
  }

  formatRemainingTime() {
    const testStore = this.props.testStore;
    const nowTime = Math.round(Date.now() / 1000);
    const remainingTime = testStore.secondsLeft - (nowTime - testStore.createdAt);
    if (remainingTime === 0) {
      logger.sendLog('Time is up', '0 seconds left');
    }
    this.setState({formatSeconds: this.formatTime(remainingTime)});
  }

  componentWillMount() {
    this.props.testStore.fetchTest().then(() => {
      this.startTimer();
      this.startHB();
    }).catch(() => {
      Alert.alert('Load error', 'There was an error while loading your test');
    });
  }

  render() {
    const {test, isSubmitting} = this.props.testStore;
    if (!test || isSubmitting) {
      return (
        <View style={styles.loading}>
          <StatusBar barStyle="dark-content" />
          <ActivityIndicator size="large" />
          <Text>{isSubmitting ? 'Submitting answers...' : 'Loading your test...'}</Text>
        </View>
      );
    }

    const {currentQuestionNum, totalQuestionNumber} = this.props.testStore;

    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <View style={styles.header}>
          <Text
            style={styles.studentName}
          >
            {test.student}
          </Text>
          <View style={styles.headerContent}>
            <Button
              title="Previous"
              disabled={currentQuestionNum <= 1 || this.props.testStore.isQuestionLoading}
              onPress={() => this.previousQuestion()}
              color={colors.textPrimary}
            />
            <Text
              style={styles.headerText}
            >
              {currentQuestionNum} / {totalQuestionNumber}
            </Text>
            <Button
              title="Next"
              disabled={currentQuestionNum >= totalQuestionNumber || this.props.testStore.isQuestionLoading}
              onPress={() => this.nextQuestion()}
              color={colors.textPrimary}
            />
          </View>
        </View>

        <View style={styles.main}>
          <Question
            question={this.props.testStore.currentQuestion}
            selectedAnswers={this.props.testStore.currentAnswers}
            isLoading={this.props.testStore.isQuestionLoading}
            onAnswerClick={(answerNumber) => this.props.testStore.selectAnswer(answerNumber)}
          />
        </View>

        <View style={styles.footer}>
          <Text
            style={[styles.timer, this.props.testStore.remainingTime <= 0 ? styles.red : null]}
          >
            {this.state.formatSeconds}
          </Text>
          <Button
            title="Submit"
            onPress={() => this.submitTest()}
            color={colors.textPrimary}
          />
        </View>
      </View>
    );
  }
}
