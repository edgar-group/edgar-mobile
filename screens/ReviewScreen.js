import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, Button, ActivityIndicator, Alert } from 'react-native';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react/native';

import colors from '../styles/colors';
import Question from '../components/Question';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignSelf: 'stretch'
  },

  loading: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  },

  header: {
    marginTop: 20,
    flexDirection: 'column'
  },

  studentName: {
    textAlign: 'center'
  },

  headerContent: {
    height: 50,
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.borderPrimary,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },

  headerText: {
    fontSize: 18
  },

  main: {
    flex: 1
  },

  footer: {
    height: 50,
    padding: 10,
    marginBottom: 10,
    borderTopWidth: 0.5,
    borderTopColor: colors.borderPrimary,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },

  submit: {
    flex: 1
  },

  score: {
    flex: 2
  }
});

@inject('testStore', 'reviewStore')
@observer
export default class ReviewScreen extends Component {
  previousQuestion() {
    this.props.reviewStore.goToPreviousQuestion();
  }

  nextQuestion() {
    this.props.reviewStore.goToNextQuestion();
  }

  finishReview() {
    this.props.testStore.reset();
    this.props.reviewStore.reset();
  }

  componentWillMount() {
    this.props.reviewStore.fetchReview();
  }

  render() {
    const {review} = this.props.reviewStore;
    if (!review) {
      return (
        <View style={styles.loading}>
          <StatusBar barStyle="dark-content" />
          <ActivityIndicator size="large" />
          <Text>Loading your review...</Text>
        </View>
      );
    }

    const {currentQuestionNum, totalQuestionNumber} = this.props.reviewStore;

    const currentQuestion = this.props.reviewStore.currentQuestion;

    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <View style={styles.header}>
          <Text
            style={styles.studentName}
          >
            {review.student}
          </Text>
          <View style={styles.headerContent}>
            <Button
              title="Previous"
              disabled={currentQuestionNum <= 1 || this.props.reviewStore.isQuestionLoading}
              onPress={() => this.previousQuestion()}
              color={colors.textPrimary}
            />
            <Text
              style={styles.headerText}
            >
              {currentQuestionNum} / {totalQuestionNumber}
            </Text>
            <Button
              title="Next"
              disabled={currentQuestionNum >= totalQuestionNumber || this.props.reviewStore.isQuestionLoading}
              onPress={() => this.nextQuestion()}
              color={colors.textPrimary}
            />
          </View>
        </View>

        <View style={styles.main}>
          <Question
            question={currentQuestion}
            selectedAnswers={currentQuestion && currentQuestion.correctAnswers || []}
            wrongAnswers={currentQuestion && currentQuestion.studentAnswers || []}
            isLoading={this.props.reviewStore.isQuestionLoading}
          />
        </View>

        <View style={styles.footer}>
          <Text
            style={styles.score}
            numberOfLines={2}
          >
            Score:{review.score} (={review.scorePerc * 100}%), Correct:{review.correctNo}, Incorrect:{review.incorrectNo}, Unanswered:{review.unansweredNo}
          </Text>
          <Button
            style={styles.submit}
            title="Done"
            onPress={() => this.finishReview()}
            color={colors.textPrimary}
          />
        </View>
      </View>
    );
  }
}
