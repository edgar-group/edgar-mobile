export const API_URL = 'https://edgar.fer.hr/';
// export const API_URL = 'http://192.168.1.2:1337';
// export const API_URL = 'http://localhost:1337';

export const PIN_LOGIN_ENABLED = true;
export const TOKEN_STORAGE_KEY = '@EdgarSessionStore:token';
