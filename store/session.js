import { AsyncStorage, Alert } from 'react-native';
import { observable, computed, action } from 'mobx';

import { post, get } from '../services/request';
import { TOKEN_STORAGE_KEY } from '../config';

class SessionStore {
  @observable user = '';
  @observable isLoaded = false;
  @observable isLoading = false;

  @action async fetchCurrentUser() {
    this.isLoading = true;
    try {
      const {user} = await get('/auth/profile');
      this.user = user;
      this.isLoading = false;
      this.isLoaded = true;
    } catch (e) {
      if (e.status === 401) {
        const token = await AsyncStorage.getItem(TOKEN_STORAGE_KEY);
        if (token) {
          await this.tokenLogin(token);
        }
        this.isLoading = false;
      } else if (e.message === 'Network request failed') {
        Alert.alert('Network request failed', 'There is a problem with connecting to server. Make sure you are connected to internet');
      } else {
        Alert.alert('An error occured', 'An error occured while trying to log you in. Try to restart application');
      }
    }
  }

  @action login({username, password}) {
    return post('/auth/locallogin', {
      data: {username, password}
    }).then(({user}) => {
      this.user = user;
    });
  }

  @action async pinLogin({pin}) {
    let token;
    try {
      const response = await post('/auth/token-register', {
        data: {pin}
      });
      token = response.token;
    } catch (e) {
      if (e.message === 'Network request failed') {
        Alert.alert('Network request failed', 'There is a problem with connecting to server. Make sure you are connected to internet');
      } else if (e.status === 404) {
        Alert.alert('Login failed', 'You entered wrong PIN');
      } else {
        Alert.alert('An error occured', 'An error occured while trying to log you in. Try to restart application');
      }
      return;
    }

    // Save token
    try {
      await AsyncStorage.setItem(TOKEN_STORAGE_KEY, token);
    } catch (e) {
      Alert.alert('An error occured', 'An error occured while trying to log you in. Try to restart application');
      return;
    }

    // Login using token
    await this.tokenLogin(token);
  }

  @action async tokenLogin(token) {
    try {
      const {user} = await post('/auth/token-login', {
        data: {token}
      });
      this.user = user;
    } catch (e) {
      if (e.message === 'Network request failed') {
        Alert.alert('Network request failed', 'There is a problem with connecting to server. Make sure you are connected to internet');
      } else if (e.status === 400) {
        Alert.alert('Login failed', 'Wrong user token provided');
      } else {
        Alert.alert('An error occured', 'An error occured while trying to log you in. Try to restart application');
      }
    }
  }
}

export default new SessionStore();
