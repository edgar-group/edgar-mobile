import { observable, computed, action } from 'mobx';
import striptags from 'striptags';

import { post, get } from '../services/request';
import logger from '../services/logger';

class TestStore {
  @observable test = null;
  @observable testInstance = null;
  @observable currentQuestionNum = 1;
  @observable createdAt = null;
  @observable testReady = false;
  @observable isQuestionLoading = false;
  @observable isSubmitting = false;
  @observable isSubmitted = false;

  @observable questions = observable.map({});
  @observable selectedAnswers = observable.map({});

  @computed get totalQuestionNumber() {
    return this.test ? this.test.noOfQuestions : null;
  }

  @computed get secondsLeft() {
    return this.test ? this.test.secondsLeft : null;
  }

  @computed get currentQuestion() {
    return this.questions.get(this.currentQuestionNum);
  }

  @computed get currentAnswers() {
    return this.selectedAnswers.get(this.currentQuestionNum);
  }

  @action fetchTest() {
    logger.sendLog('Get test', '-');
    return get('/test/instance').then((test) => {
      if (test.success === false) {
        logger.sendLog('Received NULL test', test);
        return null;
      }
      logger.sendLog('Received test', test);
      this.isQuestionLoading = true;
      this.test = test;
      this.createdAt = Math.round(Date.now() / 1000);
      return this.fetchCurrentQuestion();
    });
  }

  @action fetchCurrentQuestion() {
    if (this.currentQuestion) {
      return this.currentQuestion;
    }

    this.isQuestionLoading = true;
    return this.fetchQuestion(this.currentQuestionNum).then((question) => {
      const {ordinal} = question;
      if (ordinal) {
        this.questions.set(ordinal, question);
        this.selectedAnswers.set(ordinal, []);
      }
      this.isQuestionLoading = false;
    });
  }

  @action startTest({password}) {
    this.isSubmitted = false;
    return post('/test/new', {
      data: {password}
    }).then((response) => {
      this.testInstance = response.test_instance;
      this.testReady = true;
    });
  }

  @action selectAnswer(answerOrdinal) {
    const index = this.currentAnswers.findIndex((value) => value === answerOrdinal);
    if (index > -1) {
      this.currentAnswers.remove(answerOrdinal);
    } else {
      if (!this.currentQuestion.multiCorrect) {
        this.currentAnswers.clear();
      }
      this.currentAnswers.push(answerOrdinal);
    }
    logger.sendLog('Answer updated', this.currentAnswers);
  }

  @action goToNextQuestion() {
    this.currentQuestionNum++;
    logger.sendLog('Question selected', `${this.currentQuestionNum}`);
    return this.fetchCurrentQuestion();
  }

  @action goToPreviousQuestion() {
    this.currentQuestionNum--;
    logger.sendLog('Question selected', `${this.currentQuestionNum}`);
    return this.fetchCurrentQuestion();
  }

  @action fetchQuestion(questionNumber) {
    logger.sendLog('Get question', `num:${questionNumber}`);
    return get(`/test/question/${questionNumber}`).then((question) => {
      logger.sendLog('Received question', `num:${question.ordinal}`);
      return question;
    });
  }

  @action submit() {
    this.isSubmitting = true;
    const answers = [];
    for (let i = 1; i <= this.totalQuestionNumber; i++) {
      const selectedAnswer = this.selectedAnswers.get(i);
      answers.push(selectedAnswer && selectedAnswer.peek() || []);
    }

    logger.sendLog('Submitting test', answers);
    return post('/test/submit', {
      data: {answers}
    }).then((response) => {
      logger.sendLog('Test submitted', response);
      this.isSubmitted = true;
      this.isSubmitting = false;
    });
  }

  @action reset() {
    this.test = null;
    this.testInstance = null;
    this.currentQuestionNum = 1;
    this.createdAt = null;
    this.testReady = false;
    this.isQuestionLoading = false;
    this.isSubmitting = false;
    this.isSubmitted = false;

    this.questions = observable.map({});
    this.selectedAnswers = observable.map({});
  }
}

export default new TestStore();
