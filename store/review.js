import { observable, computed, action } from 'mobx';
import striptags from 'striptags';

import { post, get } from '../services/request';

import testStore from './test';

class ReviewStore {
  @observable currentQuestionNum = 1;
  @observable review = null;
  @observable isQuestionLoading = true;

  @observable questions = observable.map({});
  @observable selectedAnswers = observable.map({});

  @computed get totalQuestionNumber() {
    return this.review ? this.review.questionsNo : null;
  }

  @computed get currentQuestion() {
    return this.questions.get(this.currentQuestionNum);
  }

  @computed get currentAnswers() {
    return this.selectedAnswers.get(this.currentQuestionNum);
  }

  @action fetchReview() {
    const instanceID = testStore.testInstance;
    return get(`/test/review/${instanceID}`)
    .then(() => get('/test/rev/instance'))
    .then((review) => {
      this.review = review;
    }).then(() => this.fetchCurrentQuestion());
  }

  @action fetchCurrentQuestion() {
    if (this.currentQuestion) {
      return this.currentQuestion;
    }

    this.isQuestionLoading = true;
    return this.fetchQuestion(this.currentQuestionNum).then((question) => {
      const {ordinal} = question;
      if (ordinal) {
        this.questions.set(ordinal, question);
        this.selectedAnswers.set(ordinal, []);
      }
      this.isQuestionLoading = false;
    });
  }

  @action goToNextQuestion() {
    this.currentQuestionNum++;
    return this.fetchCurrentQuestion();
  }

  @action goToPreviousQuestion() {
    this.currentQuestionNum--;
    return this.fetchCurrentQuestion();
  }

  @action fetchQuestion(questionNumber) {
    return get(`/test/rev/question/${questionNumber}`);
  }

  @action reset() {
    this.currentQuestionNum = 1;
    this.review = null;
    this.isQuestionLoading = true;

    this.questions = observable.map({});
    this.selectedAnswers = observable.map({});
  }
}

export default new ReviewStore();
