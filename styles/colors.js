const baseWhite = '#FFFFFF';
const baseBlue = '#2196F3';
const baseGreen = '#4CAF50';
const baseRed = '#F44336';

export default {
  // BACKGROUND
  bgPrimary: baseBlue,
  bgLight: baseWhite,
  bgImportant: baseRed,

  // TEXT
  textPrimary: baseBlue,
  textPrimaryInverted: baseWhite,
  textAffirmative: baseGreen,
  textImportant: baseRed,

  // BORDER
  borderPrimary: baseBlue,
  borderPrimaryInverted: baseWhite
};
