import React, { Component } from 'react';
import { View, Dimensions, WebView } from 'react-native';

const injectedScript = function() {
  const wrapper = document.createElement('div');
  wrapper.id = 'height-wrapper';
  while (document.body.firstChild) {
    wrapper.appendChild(document.body.firstChild);
  }

  document.body.appendChild(wrapper);

  let i = 0;
  function updateHeight() {
    document.title = wrapper.clientHeight;
    window.location.hash = ++i;
  }

  updateHeight();

  window.addEventListener('load', function() {
    updateHeight();
    setTimeout(updateHeight, 1000);
  });

  window.addEventListener('resize', updateHeight);
};

export default class ResizableWebView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      realContentHeight: 0
    };
  }

  handleNavigationChange(navState) {
    if (navState.title) {
      const realContentHeight = parseInt(navState.title, 10) || 0; // turn NaN to 0
      this.setState({realContentHeight: realContentHeight + 24});
    }
  }

  render() {
    const _w = Dimensions.get('window').width;
    const _h = this.state.realContentHeight;

    const injectedScriptString = String(injectedScript);
    return (
      <WebView
        injectedJavaScript={`(${injectedScriptString})();`}
        scrollEnabled={false}
        onMessage={(e) => this.onMessage(e)}
        javaScriptEnabled={true}
        automaticallyAdjustContentInsets={true}
        onNavigationStateChange={(nav) => this.handleNavigationChange(nav)}
        {...this.props}
        style={[{width: _w}, this.props.style, {height: _h}]}
      />
    );
  }
}
