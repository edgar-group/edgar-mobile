import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, ActivityIndicator, ScrollView } from 'react-native';
import ResizableWebView from './ResizableWebView';
import {API_URL} from '../config';

import colors from '../styles/colors';

const defaultInjectedStyling = `
html {
  font-size: 16px;
}

body {
  font-family: 'Roboto Regular',sans-serif;
  margin-left: 5px;
  margin-right: 5px;
}

div.profilepic {
   width: 70px;
   height:70px;
   margin-left: 10px;
   margin-right:10px;
}

div.profilepic img {
    width: 100%;
    height: 100%;
    object-fit: contain;
}

div.profilepicbig {
   width: 101px;
   height:101px;
   margin-left: 2px;
   margin-right:2px;
}

div.profilepicbig img {
    width: 100%;
    height: 100%;
    object-fit: contain;
}

div.modal-header {
  background-image: url(../images/edgar_confirm.png);
  background-repeat: no-repeat;
  min-height: 150px;
  padding-left: 125px;
  padding-top: 25px;
}

.navbar-main {
  border-bottom: #9de5fb 5px solid;
}
/* signin: */
.graybg {
    background-color:#f5f5f5;
}
.form-signin
{
    margin-top: 200px;
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}

.table td.aii-code {
  font-family: "Lucida Console", Monaco, monospace;
  padding: 1px;
  border: 1px solid #eceeef;
}

.table td.aii-code-header {
  cursor: pointer;
}

.aii-error {
    color: red;
    font-style: italic;
}

.aii-warning {
    color: orange;
    font-style: italic;
}
.aii-clock-error {
    background-color: red;
}

.aii-clock-warning {
    background-color: orange;
}

/*// signin*/


.aii-qn-tag {
  margin-left: 0.5rem;
  font-size: 2rem;
}
.text-green {
  color: green;
}
.text-red {
  color: red;
}
.text-gray {
  color: gray;
}
.text-yellow {
  color: yellow;
}


/* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
  text-align: center;
}
/* /Sticky footer styles
-------------------------------------------------- */


/*
 * Sidebar, from: http://v4-alpha.getbootstrap.com/examples/dashboard/
 */

/* Hide for mobile, show later */
.sidebar {
  display: none;
}
@media (min-width: 768px) {
  .sidebar {
    position: fixed;
    top: 75px;
    bottom: 0;
    left: 0;
    z-index: 0;
    display: block;
    padding: 40px;
    overflow-x: hidden;
    overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */

    border-right: 1px solid #eee;
  }
}

/* Sidebar navigation */
.nav-sidebar {
  margin-right: -21px; /* 20px padding + 1px border */
  margin-bottom: 20px;
  margin-left: 15px;
  background-color: #f5f5f5;
}
.nav-sidebar > li > a {
  padding-right: 20px;
  padding-left: 20px;
}
.nav-sidebar > .active > a,
.nav-sidebar > .active > a:hover,
.nav-sidebar > .active > a:focus {
  color: #fff;
  background-color: #428bca;
}


/*
 * Main content
 */

.main {
  padding: 20px;
}
@media (min-width: 768px) {
  .main {
    padding-right: 40px;
    padding-left: 40px;
  }
}
.main .page-header {
  margin-top: 0;
}

/*
 * /Sidebar
 */



.CodeMirror {
  height: auto;
}

.ra {
  text-align: right;
}

/*
 * Markdown styled CSS, taken from view-source:https://jbt.github.io/markdown-editor/, TODO: minimize
 */
.CodeMirror pre{
      line-height: 16px;
    }

a{ color: #0645ad; text-decoration:none;}
a:visited{ color: #0b0080; }
a:hover{ color: #06e; }
a:active{ color:#faa700; }
a:focus{ outline: thin dotted; }
a:hover, a:active{ outline: 0; }


pre, code{
  color: #000;
  font-family:Consolas, "Liberation Mono", Menlo, Courier, monospace;
  font-size: 0.94em; /* 0.94 = 0.88 + (1.00 - 0.88) / 2 */
  border-radius:3px;
  background-color: #F8F8F8;
  border: 1px solid #CCC;
}
pre { white-space: pre; white-space: pre-wrap; word-wrap: break-word; padding: 5px;}
pre code { border: 0px !important; background: transparent !important; line-height: 1.3em; }
code { padding: 0 3px 0 3px; }


.toggle-off.btn {
    padding-left: 24px;
    border: 1px solid red;
    background: orange;
}


video {
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    transform: translateX(-50%) translateY(-50%);
 background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;
  background-size: cover;
  transition: 1s opacity;
}


/* md styles: */
blockquote{color:#666666;margin:0;padding-left: 3em;border-left: 0.5em #EEE solid;}
    hr { display: block; height: 2px; border: 0; border-top: 1px solid #aaa;border-bottom: 1px solid #eee; margin: 1em 0; padding: 0; }

    pre, code{
      color: #000;
      font-family:Consolas, "Liberation Mono", Menlo, Courier, monospace;
      font-size: 0.94em; /* 0.94 = 0.88 + (1.00 - 0.88) / 2 */
      border-radius:3px;
      background-color: #F8F8F8;
      border: 1px solid #CCC;
    }
    pre { white-space: pre; white-space: pre-wrap; word-wrap: break-word; padding: 5px;}
    pre code { border: 0px !important; background: transparent !important; line-height: 1.3em; }
    code { padding: 0 3px 0 3px; }
    sub, sup { font-size: 75%; line-height: 0; position: relative; vertical-align: baseline; }
    sup { top: -0.5em; }
    sub { bottom: -0.25em; }
    /*ul, ol { margin: 1em 0; padding: 0 0 0 2em; }*/
    li p:last-child { margin:0 }
    dd { margin: 0 0 0 2em; }
    img { border: 0; -ms-interpolation-mode: bicubic; vertical-align: middle; }
table { border-collapse: collapse; border-spacing: 0; }
    th { vertical-align: top; padding: 4px 10px; border: 1px solid #bbb; background-color: #ffdb99;}
    td { vertical-align: top; padding: 4px 10px; border: 1px solid #bbb; }
    tr:nth-child(even) td, tr:nth-child(even) th { background: #eee; }

table.edgar th { vertical-align: top; padding: 4px 10px; border: 1px solid #bbb; background-color: aliceblue;}
`;

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  },

  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignSelf: 'stretch'
  },

  content: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    flex: 1
  },

  contentWebView: {
    flex: 1,
    alignSelf: 'stretch'
  },

  question: {
    borderColor: colors.borderPrimary,
    borderRadius: 8,
    borderWidth: 1,
    margin: 10,
    padding: 10
  },

  answer: {
    margin: 10,
    flexDirection: 'row'
  },

  webView: {
    backgroundColor: 'transparent'
  }
});

const style = `<style>${defaultInjectedStyling} img{max-width: 100%}</style>`;

export default class Question extends Component {
  onAnswerClick(answerOrdinal) {
    if (this.props.onAnswerClick) {
      this.props.onAnswerClick(answerOrdinal);
    }
  }

  render() {
    const isLoading = this.props.isLoading;
    if (isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" />
          <Text>Loading your question...</Text>
        </View>
      );
    }

    const currentQuestion = this.props.question;
    const currentAnswers = this.props.selectedAnswers;
    const wrongAnswers = this.props.wrongAnswers || [];
    const labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's'];

    const answers = currentQuestion.answers
      .sort((itemA, itemB) => itemA.ordinal - itemB.ordinal)
      .map((answer) => {
        const wrongAnswer = wrongAnswers.indexOf(answer.ordinal) > -1;
        const answerSelected = currentAnswers.indexOf(answer.ordinal) > -1;
        const answerLabel = labels[answer.ordinal - 1];
        const wrongColor = wrongAnswer ? colors.textImportant : colors.textPrimary;
        const color = answerSelected ? colors.textAffirmative : wrongColor;

        return (
          <View
            style={styles.answer}
            key={answer.ordinal}
          >
            <Button
              style={styles.answerButton}
              color={color}
              title={answerLabel}
              onPress={() => this.onAnswerClick(answer.ordinal)}
            />

            <ResizableWebView
              style={styles.webView}
              source={{html: style + answer.aHtml, baseUrl: API_URL}}
              bounces={false}
              startInLoadingState={true}
            />
          </View>
        );
      });

    return (
      <ScrollView style={styles.container}>
        <ResizableWebView
          style={styles.webView}
          source={{html: style + currentQuestion.content, baseUrl: API_URL}}
          startInLoadingState={true}
        />

        {answers}
      </ScrollView>
    );
  }
}
