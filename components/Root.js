// For testing purposes
import 'babel-polyfill';
// import * as Expo from 'expo';

import React, { Component } from 'react';
import { StatusBar, View, StyleSheet } from 'react-native';
import { Provider } from 'mobx-react';

import testStore from '../store/test';
import sessionStore from '../store/session';
import reviewStore from '../store/review';

import App from './App';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center'
  }
});

export default class edgar extends Component {
  render() {
    return (
      <Provider
        sessionStore={sessionStore}
        testStore={testStore}
        reviewStore={reviewStore}
      >
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <App />
        </View>
      </Provider>
    );
  }
}
