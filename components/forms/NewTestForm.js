import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View, Button, Alert } from 'react-native';
import { inject } from 'mobx-react';

import colors from '../../styles/colors';

const styles = StyleSheet.create({
  form: {
    minWidth: 200
  },
  field: {
    marginTop: 20
  },
  inputWrapper: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.borderPrimary
  },
  label: {
    color: colors.textPrimary
  },
  input: {
    marginTop: 5,
    height: 26,
    fontSize: 16,
    padding: 4,
    color: colors.textPrimary
  },
  submit: {
    marginTop: 40
  }
});

@inject('testStore')
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      testPassword: ''
    };
  }

  submit() {
    this.props.testStore.startTest({
      password: this.state.testPassword
    }).catch((e) => {
      if (e.status === 404) {
        Alert.alert('No test', 'There is no test for given password');
        this.setState({testPassword: ''});
      } else {
        throw e;
      }
    });
  }

  render() {
    return (
      <View style={styles.form}>
        <View style={[styles.field, styles.inputWrapper]}>
          <TextInput
            style={styles.input}
            onChangeText={(testPassword) => this.setState({testPassword})}
            value={this.state.testPassword}
            underlineColorAndroid='transparent'
            autoCapitalize='none'
            placeholder='Test password'
          />
        </View>

        <View style={[styles.field, styles.submit]}>
          <Button
            title="Start test"
            disabled={!this.state.testPassword}
            onPress={() => this.submit()}
            color={colors.textPrimary}
          />
        </View>
      </View>
    );
  }
}
