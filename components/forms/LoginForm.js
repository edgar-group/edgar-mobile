import React, { Component } from 'react';
import { Platform, StyleSheet, TextInput, Text, View, Button, Alert } from 'react-native';
import { inject } from 'mobx-react';

import colors from '../../styles/colors';

const styles = StyleSheet.create({
  form: {
    minWidth: 200
  },
  field: {
    marginTop: 20
  },
  noSpacing: {
    marginTop: 0
  },
  inputWrapper: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.borderPrimaryInverted
  },
  label: {
    color: colors.textPrimaryInverted
  },
  input: {
    marginTop: 5,
    height: 26,
    fontSize: 16,
    padding: 4,
    color: colors.textPrimaryInverted
  },
  submit: {
    marginTop: 40
  }
});

@inject('sessionStore')
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  login() {
    this.props.sessionStore.login({
      username: this.state.username,
      password: this.state.password
    }).catch((e) => {
      if (e.message === 'Network request failed') {
        Alert.alert('Network request failed', 'There is a problem with connecting to server. Make sure you are connected to internet');
      } else if (e.status === 400) {
        Alert.alert('Login failed', 'You entered wrong username or password');
        this.setState({password: ''});
      } else {
        Alert.alert('An error occured', 'An error occured while trying to log you in. Try to restart application');
      }
    });
  }

  render() {
    return (
      <View style={styles.form}>
        <View style={[styles.field, styles.noSpacing, styles.inputWrapper]}>
          <Text style={styles.label}>USERNAME</Text>
          <TextInput
            style={styles.input}
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            keyboardType='email-address'
            underlineColorAndroid='transparent'
            autoCapitalize='none'
          />
        </View>

        <View style={[styles.field, styles.inputWrapper]}>
          <Text style={styles.label}>PASSWORD</Text>
          <TextInput
            style={styles.input}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            secureTextEntry={true}
            underlineColorAndroid='transparent'
            autoCapitalize='none'
          />
        </View>

        <View style={[styles.field, styles.submit]}>
          <Button
            title="Login"
            disabled={!this.state.username || !this.state.password}
            onPress={() => this.login()}
            color={Platform.OS === 'ios' ? colors.textPrimaryInverted : colors.textPrimary}
          />
        </View>
      </View>
    );
  }
}
