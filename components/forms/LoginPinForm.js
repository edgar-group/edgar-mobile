import React, { Component } from 'react';
import { Platform, StyleSheet, TextInput, Text, View, Button, Alert } from 'react-native';
import { inject } from 'mobx-react';

import colors from '../../styles/colors';

const styles = StyleSheet.create({
  form: {
    minWidth: 200
  },

  field: {
    marginTop: 20
  },

  noSpacing: {
    marginTop: 0
  },

  inputWrapper: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.borderPrimaryInverted
  },

  label: {
    color: colors.textPrimaryInverted
  },

  input: {
    marginTop: 5,
    height: 26,
    fontSize: 16,
    padding: 4,
    color: colors.textPrimaryInverted
  },

  submit: {
    marginTop: 40
  }
});

@inject('sessionStore')
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pin: ''
    };
  }

  login() {
    this.props.sessionStore.pinLogin({
      pin: this.state.pin
    });
  }

  render() {
    return (
      <View style={styles.form}>
        <View style={[styles.field, styles.noSpacing, styles.inputWrapper]}>
          <Text style={styles.label}>PIN</Text>
          <TextInput
            style={styles.input}
            onChangeText={(pin) => this.setState({pin})}
            value={this.state.pin}
            keyboardType='numeric'
            underlineColorAndroid='transparent'
            autoCapitalize='none'
          />
        </View>

        <View style={[styles.field, styles.submit]}>
          <Button
            title="Login"
            disabled={!this.state.pin}
            onPress={() => this.login()}
            color={Platform.OS === 'ios' ? colors.textPrimaryInverted : colors.textPrimary}
          />
        </View>
      </View>
    );
  }
}
