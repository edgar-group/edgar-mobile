import React, { Component } from 'react';
import { View } from 'react-native';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react/native';

import MainScreen from '../screens/MainScreen';
import TestScreen from '../screens/TestScreen';
import ReviewScreen from '../screens/ReviewScreen';

@inject('testStore')
@observer
export default class Navigation extends Component {

  render() {
    if (this.props.testStore.isSubmitted) {
      return (
        <ReviewScreen />
      );
    }
    
    if (this.props.testStore.testReady) {
      return (
        <TestScreen />
      );
    }

    return (
      <MainScreen />
    );
  }
}
