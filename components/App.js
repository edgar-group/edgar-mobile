import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react/native';

import LoginScreen from '../screens/LoginScreen';
import AppNavigation from './AppNavigation';

import colors from '../styles/colors';

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    backgroundColor: colors.bgPrimary,
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

@inject('sessionStore')
@observer
export default class App extends Component {
  constructor(props) {
    super(props);
    if (!this.props.sessionStore.user && !this.props.sessionStore.isLoaded) {
      this.props.sessionStore.fetchCurrentUser();
    }
  }

  render() {
    if (this.props.sessionStore.isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color={colors.bgLight} />
        </View>
      );
    }

    if (!this.props.sessionStore.user) {
      return <LoginScreen />;
    }
    return <AppNavigation />;
  }
}
