import 'babel-polyfill';
import { AppRegistry } from 'react-native';

import edgar from './components/Root';

AppRegistry.registerComponent('edgar', () => edgar);
